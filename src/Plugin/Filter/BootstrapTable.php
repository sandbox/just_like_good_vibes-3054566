<?php

namespace Drupal\bootstrap_table_filter\Plugin\Filter;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\Html;
use Drupal\filter\FilterProcessResult;
use Drupal\filter\Plugin\FilterBase;

/**
 * A filter that adds bootstrap table classes to html tables
 *
 * @Filter(
 *   id = "bootstrap_table_filter",
 *   title = @Translation("Add bootstrap classes to HTML tables"),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_TRANSFORM_REVERSIBLE,
 *   settings = {
 *       "classes" = "bootstrap-table",
 *       "table_striped" = FALSE,
 *       "table_hover" = TRUE,
 *       "table_bordered" = TRUE
 *   },
 *   weight = -10
 * )
 */
class BootstrapTable extends FilterBase {


  /**
   * Return a table describing the association between config option keys and associated bootstrap classes
   *
   * @return array
   */
  protected function getConfigToBootstrapClasses() {
		$class_textes = [
      "table_striped" => [
        "class" => "table-striped"
      ],
      "table_hover" => [
        "class" => "table-hover"
      ],
      "table_bordered" => [
        "class" => "table-bordered"
      ],
    ];
		return $class_textes;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form['classes'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Add classes to HTML tables'),
      '#default_value' => $this->settings['classes'],
      '#description' => $this->t('A list of classes to add to all tables, separated by spaces'),
    ];

    foreach($this->getConfigToBootstrapClasses() as $config_name => $class_data) {
        $class_name = $class_data["class"];
        $description = array_key_exists("description", $class_data)?
          $class_data["description"] : "";
        $form[$config_name] = [
            '#type' => 'checkbox',
            '#title' => $this->t('Add bootstrap class @class to HTML tables', ["@class" => $class_name]),
            '#description' => $description,
            '#default_value' => $this->settings[$config_name]
        ];
    }
    return $form;
  }

  protected function string_to_classes($input_string) {
    $input_string = str_replace("\t"," ",$input_string);
    $input_string = str_replace("\n"," ",$input_string);
    $input_string = str_replace(","," ",$input_string);
    $input_string = str_replace("|"," ",$input_string);
    while( strpos($input_string, "  ") !== false) {
      $input_string = str_replace("  "," ",$input_string);
    }
    $classes_table = explode(' ', $input_string);
    return array_combine($classes_table, $classes_table);
  }

  /**
   * add classes to DOMElement
   *
   * @param $tag_element
   * @param $classes
   */
  protected function addClasses(\DOMElement $tag_element, $classes) {
      $definedClasses = $this->string_to_classes($tag_element->getAttribute('class'));
      $classes_to_add = [];
      foreach($classes as $className) {
          $clean_class = Html::getClass($className);
          if (!array_key_exists($clean_class, $definedClasses)) {
              $definedClasses[$clean_class] = $clean_class;
              $classes_to_add []= $clean_class;
          }
      }
      if(count($classes_to_add) > 0) {
        $existing_classes = "".$tag_element->getAttribute('class');
        $new_classes = (strlen($existing_classes) > 0)?
          $existing_classes.' ' . implode(" ", $classes_to_add) : implode(" ", $classes_to_add);
        $tag_element->setAttribute('class', $new_classes);
        return TRUE;
      }
      return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function process($text, $langcode) {
    $document = Html::load($text);
    $table_elements =  $document->getElementsByTagName("table");
    $classes_to_add =  $this->string_to_classes($this->settings["classes"]);
    foreach($this->getConfigToBootstrapClasses() as $config_key => $class_data) {
        $class_name = $class_data["class"];
        if($this->settings[$config_key]) {
          $classes_to_add[$class_name] = $class_name;
        }
    }
    if(count($classes_to_add) > 0) {
      foreach ($table_elements as $tag_element) {
        $this->addClasses($tag_element, array_values($classes_to_add));
      }
    }
    return new FilterProcessResult(Html::serialize($document));
  }
}


